#include <Python.h>

namespace contrib
{

void initpy(const char *cselfpath);
PyObject *pyglobals();

} /* namespace contrib */
